package com.example.useritem.controller;

import com.example.useritem.request.AddItemRequest;
import com.example.useritem.request.UpdateItemRequest;
import com.example.useritem.response.UpdateResponse;
import com.example.useritem.service.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController(value = "item")
public class ItemController {

    private ItemService itemService;

    @Autowired
    public ItemController(ItemService itemService){
        this.itemService = itemService;
    }

    @PostMapping(value = "/item")
    public ResponseEntity add(@RequestBody AddItemRequest request){
        validateDescriptionSize(request.getDescription());
        UpdateResponse updateResponse = itemService.add(request.getUserId(), request.getDescription());
        return ResponseEntity.ok(updateResponse);
    }

    @PutMapping(value = "/item")
    public ResponseEntity update(@RequestBody UpdateItemRequest request){
        UpdateResponse updateResponse = itemService.update(request.getId());
        return ResponseEntity.ok(updateResponse);
    }

    @DeleteMapping(value = "/item")
    public ResponseEntity delete(@RequestBody UpdateItemRequest request){
        UpdateResponse updateResponse = itemService.delete(request.getId());
        return ResponseEntity.ok(updateResponse);
    }

    private void validateDescriptionSize(String value){
        if (value.isEmpty() || value.length() >60){
            throw new IllegalArgumentException();
        }
    }
}
