package com.example.useritem.controller;

import com.example.useritem.entity.Item;
import com.example.useritem.response.UpdateResponse;
import com.example.useritem.request.AddUserRequest;
import com.example.useritem.service.ItemService;
import com.example.useritem.service.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class UserController {

    private UserService userService;
    private ItemService itemService;

    public UserController(UserService userService, ItemService itemService) {
        this.userService = userService;
        this.itemService = itemService;
    }

    @PostMapping(value = "/user")
    public ResponseEntity addUser(@RequestBody AddUserRequest request) throws IllegalAccessException {
        validateName(request.getName());
        UpdateResponse addUserResponse = userService.add(request.getName());
        return ResponseEntity.ok(addUserResponse);
    }

    @GetMapping(value = "/user/{id}/item")
    public ResponseEntity retrieveItemsByUserId(@PathVariable("id") Long userId){
        List<Item> items = itemService.retrieveItemsByUserId(userId);
        return ResponseEntity.ok(items);
    }

    private void validateName(String name) throws IllegalAccessException {
        if(name.isEmpty() || name.length() > 24){
            throw new IllegalAccessException();
        }
    }






}
