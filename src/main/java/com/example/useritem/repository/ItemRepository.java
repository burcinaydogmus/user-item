package com.example.useritem.repository;

import com.example.useritem.entity.Item;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface ItemRepository extends JpaRepository<Item, Long> {

    Optional<Item> findById(Long id);

    List<Item> findByUserId(Long userId);

    void deleteById(Long id);
}
