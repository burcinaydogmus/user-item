package com.example.useritem.service;

import com.example.useritem.entity.User;
import com.example.useritem.response.UpdateResponse;
import org.springframework.stereotype.Service;
import com.example.useritem.repository.UserRepository;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    private UserRepository userRepository;

    public UserService(UserRepository userRepository){
        this.userRepository = userRepository;
    }

    public UpdateResponse add(String name){
        User user = new User();
        user.setName(name);
        userRepository.save(user);
        return new UpdateResponse("", "Success");
    }

    public Optional<User> retrieveUserById(Long id){
        return userRepository.findById(id);
    }

    public List<String> getUsernames(){
        List<String> usernames = new ArrayList<String>();
        Iterator iterator = getUsers().iterator();

        while (iterator.hasNext()){
            User user = (User) iterator.next();
            usernames.add(user.getName());
        }
        return usernames;
    }

    public Iterable<User> getUsers(){
        return userRepository.findAll();
    }
}
