package com.example.useritem.service;

import com.example.useritem.entity.Item;
import com.example.useritem.entity.User;
import com.example.useritem.repository.ItemRepository;
import com.example.useritem.repository.UserRepository;
import com.example.useritem.response.UpdateResponse;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ItemService {
    private ItemRepository itemRepository;
    private UserRepository userRepository;

    public ItemService(ItemRepository itemRepository){
        this.itemRepository = itemRepository;
        this.userRepository = userRepository;
    }

    public UpdateResponse add(Long id, String description){
        Item item = new Item();
        item.setDescription(description);
        item.setStock(2);
        item.setUser(userRepository.findById(id).get());
        itemRepository.save(item);

        return new UpdateResponse("item id:" + item.getId() + "has been created successfully", "Success");
    }

    public UpdateResponse update(Long id){
        UpdateResponse updateResponse = new UpdateResponse();
        Optional<Item> optionalItem = itemRepository.findById(id);

        if (optionalItem.isPresent()){
            Item item = optionalItem.get();
            item.setStock(1);
            itemRepository.save(item);
            updateResponse.setMessage(item.getDescription() + " has been marked as complete");
            updateResponse.setStock("Success");
        }
        else {
            updateResponse.setMessage("Failure ");
        }

        return updateResponse;
    }
    public UpdateResponse delete(Long id){
        UpdateResponse updateResponse = new UpdateResponse();
        Optional<Item> optionalItem = itemRepository.findById(id);

        if (optionalItem.isPresent()){
            Item item = optionalItem.get();
            itemRepository.delete(item);
            updateResponse.setMessage("item: (" + item.getDescription() + " : " + item.getStockStr() + ") has been deleted successfully");
            updateResponse.setStock("Success");
        }
        else{
            updateResponse.setMessage("Failure");

        }
        return updateResponse;
    }

    public List<Item> retrieveItemsByUserId(Long userId){
        return itemRepository.findByUserId(userId);
    }

    public void assignItem(String name, Long id){
        User user = new User();
    }

    public Iterable<Item> getItems(){
        return itemRepository.findAll();
    }

}
