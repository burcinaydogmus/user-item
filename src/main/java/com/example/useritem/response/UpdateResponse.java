package com.example.useritem.response;

public class UpdateResponse {
    private String message;
    private String stock;

    public UpdateResponse(){

    }

    public UpdateResponse(String message, String stock){
        this.message = message;
        this.stock = stock;

    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStock() {
        return stock;
    }

    public void setStock(String stock) {
        this.stock = stock;
    }
}

